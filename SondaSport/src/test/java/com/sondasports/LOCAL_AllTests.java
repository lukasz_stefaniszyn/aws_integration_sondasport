package com.sondasports;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.sondasports.account.db.DataBaseConnectionTest;
import com.sondasports.account.db.model.DBItemModelTest;
import com.sondasports.account.lambda.DBitemActivateTokenTest;
import com.sondasports.account.lambda.DBitemDeleteEmailLocalTest;
import com.sondasports.account.lambda.DBitemGetEmailByTokenLocalTest;
import com.sondasports.account.lambda.DBitemGetEmailLocalTest;
import com.sondasports.account.lambda.DBitemUpdateEmailLocalTest;
import com.sondasports.account.ses.SimpleEmailSenderTest;
import com.sondasports.credentials.GetAWSCredentialsTest;

@RunWith(Suite.class)
@SuiteClasses({ 
	DBitemDeleteEmailLocalTest.class, 
	DBitemGetEmailByTokenLocalTest.class, 
	DBitemGetEmailLocalTest.class,
	DBitemUpdateEmailLocalTest.class, 
	DBItemModelTest.class, 
	DataBaseConnectionTest.class, 
	GetAWSCredentialsTest.class,
	DBitemActivateTokenTest.class,
	SimpleEmailSenderTest.class})
public class LOCAL_AllTests {

}
