package com.sondasports.account.db.utils;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.GlobalSecondaryIndex;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.sondasports.account.db.DataBaseConnection;
import com.sondasports.account.db.model.AttributeNaming;
import com.sondasports.account.db.model.DBItemModel;
import com.sondasports.account.db.settings.DynamoDbTableSettings;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public class DynamoDBCreateTable {
	
	private final static ProvisionedThroughput THRUPUT = new ProvisionedThroughput(1L, 2L);
	private final static Projection PROJECTION = new Projection()
			.withProjectionType(ProjectionType.ALL);
	
	static String TABLE_NAME = DynamoDbTableSettings.TABLE_NAME_ACCOUNT
			.toString();
	
	private static AmazonDynamoDBClient client;
	private static DynamoDB dynamoDB;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		client = DataBaseConnection
				.getLocalClient();
		// client = DataBaseConnection.getCloud_EuWest1_Client();
		dynamoDB = new DynamoDB(client);
		
		// try{
		// deleteTable(dynamoDB, TABLE_NAME);
		// } catch(Exception e) {}
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		// dynamoDB.shutdown();
	}
	
	@Before
	public void setUp() throws Exception {
		String csvFilePath = "dbItemTestData/dbItemsTestData_1_2.csv";
		Table table = createTable(dynamoDB, TABLE_NAME, csvFilePath);
		
		Item item2 = table
				.getItem(DBItemModel.HASH_KEY_NAME, "19_test@softwareautomation.pl");
		System.out
				.println("ITEM: " + item2
						.toJSONPretty()
						.toString());
	}
	
	@After
	public void tearDown() throws Exception {
		// deleteTable(dynamoDB, TABLE_NAME);
	}
	
	@Test
	public void testName() throws Exception {
		assertTrue(true);
	}
	
	public static Table createTable(DynamoDB dynamoDB, String tableName, String csvFilePath)
			throws InterruptedException, NoSuchFieldException, IOException, JsonProcessingException {
		Table table = createTable(dynamoDB, tableName);
		
		List<DBItemModel> dBItems = createDBItemsFromCSV(csvFilePath);
		
		for (DBItemModel dbItem : dBItems) {
			inputItemToDB(table, dbItem);
		}
		return table;
	}
	
	/**
	 * Sample request to create a DynamoDB table with an LSI and GSI that can be accessed via a combination of hash keys
	 * and range keys.
	 * 
	 * @return
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 */
	public static Table createTable(DynamoDB dynamoDB, String TableName)
			throws InterruptedException, NoSuchFieldException, SecurityException {
		
		Table table = DynamoDBUtils
				.getTable(dynamoDB, TableName);
		if (table == null) {
			// Table doesn't exist. Let's create it.
			table = dynamoDB
					.createTable(createTableRequest(TableName));
			// Wait for the table to become active
			TableDescription desc = table
					.waitForActive();
			System.out
					.println("Table is ready for use! " + desc);
		}
		return table;
	}
	
	public static void deleteTable(DynamoDB dynamoDB, String TableName) throws InterruptedException {
		Table table = dynamoDB
				.getTable(TableName);
		// Wait for the table to become active or deleted
		TableDescription desc = table
				.waitForActiveOrDelete();
		if (desc == null) {
			System.out
					.println("Table " + table
							.getTableName() + " does not exist.");
		} else {
			table
					.delete();
			// No need to wait, but you could
			table
					.waitForDelete();
			System.out
					.println("Table " + table
							.getTableName() + " has been deleted");
		}
	}
	
	public static void inputItemToDB(Table table, DBItemModel dBItemModel) throws JsonProcessingException {
		Item item = dBItemModel
				.getItem();
		table
				.putItem(item);
		return;
	}
	
	private static CreateTableRequest createTableRequest(String tableName)
			throws NoSuchFieldException, SecurityException {
		// primary keys
		String HASH_KEY_NAME = AttributeNaming.emailAddressName;
		// String RANGE_KEY_NAME =
		// DBItemModel.class.getDeclaredField("creationData").getName();
		
		// local secondary index
		// String LSI_NAME = "myLSI";
		// String LSI_RANGE_KEY_NAME = "myLsiRangeKey";
		
		// global secondary index
		String RANGE_GSI_NAME = AttributeNaming.tokenName;
		String GSI_HASH_KEY_NAME = AttributeNaming.tokenName;
		// String GSI_RANGE_KEY_NAME =
		// DBItemModel.class.getDeclaredField("creationData").getName();;
		
		ArrayList<AttributeDefinition> attributeDefinitions = new ArrayList<AttributeDefinition>();
		
		attributeDefinitions
				.add(new AttributeDefinition()
						.withAttributeName(HASH_KEY_NAME)
						.withAttributeType(ScalarAttributeType.S));
		
		attributeDefinitions
				.add(new AttributeDefinition()
						.withAttributeName(GSI_HASH_KEY_NAME)
						.withAttributeType(ScalarAttributeType.S));
		
		CreateTableRequest req = new CreateTableRequest()
				.withTableName(tableName)
				.withAttributeDefinitions(attributeDefinitions
				// new AttributeDefinition(GSI_RANGE_KEY_NAME,
				// ScalarAttributeType.S)
				)
				.withKeySchema(new KeySchemaElement(HASH_KEY_NAME, KeyType.HASH)
				// new KeySchemaElement(RANGE_KEY_NAME, KeyType.RANGE)
				)
				.withProvisionedThroughput(THRUPUT)
				.withGlobalSecondaryIndexes(new GlobalSecondaryIndex()
						.withIndexName(RANGE_GSI_NAME)
						.withProvisionedThroughput(THRUPUT)
						.withKeySchema(new KeySchemaElement()
								.withAttributeName(GSI_HASH_KEY_NAME)
								.withKeyType(KeyType.HASH)
						// new KeySchemaElement(GSI_RANGE_KEY_NAME, KeyType.RANGE)
						)
						.withProjection(PROJECTION)
						.withProvisionedThroughput(THRUPUT))
		// .withLocalSecondaryIndexes(
		// new LocalSecondaryIndex()
		// .withIndexName(LSI_NAME)
		// .withKeySchema(
		// new KeySchemaElement(HASH_KEY_NAME, KeyType.HASH),
		// new KeySchemaElement(LSI_RANGE_KEY_NAME, KeyType.RANGE))
		// .withProjection(PROJECTION))
		;
		return req;
	}
	
	private static List<DBItemModel> createDBItemsFromCSV(String csvFilePath)
			throws IOException, JsonProcessingException {
		CsvMapper mapper = new CsvMapper();
		CsvSchema schema = mapper
				.schemaFor(DBItemModel.class)
				.withHeader();
		schema = schema
				.withColumnSeparator(';')
				// and write Java nulls as "NULL" (instead of empty string)
				.withNullValue("NULL")
				// and let's NOT allow escaping with backslash ('\')
				.withoutEscapeChar();
		
		String resource = DynamoDBCreateTable.class
				.getClassLoader()
				.getResource(csvFilePath)
				.getFile();
		File csvFile = new File(resource);
		MappingIterator<DBItemModel> it = mapper
				.readerFor(DBItemModel.class)
				.with(schema)
				.readValues(csvFile);
		List<DBItemModel> dBItems = new ArrayList<DBItemModel>();
		DBItemModel dBItemModel = null;
		while (it
				.hasNextValue()) {
			dBItemModel = it
					.nextValue();
			dBItems
					.add(dBItemModel);
		}
		return dBItems;
	}
	
}
