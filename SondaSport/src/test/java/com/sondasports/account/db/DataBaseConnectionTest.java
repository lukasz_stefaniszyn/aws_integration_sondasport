package com.sondasports.account.db;

import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.ListTablesResult;
import com.sondasports.account.db.exceptions.DynamoDBConnectionException;

public class DataBaseConnectionTest {
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@Before
	public void setUp() throws Exception {
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Ignore
	@Test(expected = DynamoDBConnectionException.class)
	public void testLocalConnetion_whenServerDisabled() {
		System.out.println("Test started");
		AmazonDynamoDBClient localClient = DataBaseConnection.getLocalClient();;
		try {
			ListTablesResult listTables = localClient.listTables();
			System.out.println(listTables.toString());
		} catch (AmazonClientException e) {
			throw new DynamoDBConnectionException(e);
		} finally{
			try {
//				localClient.shutdown();
			} catch (AmazonClientException e) {
				throw new DynamoDBConnectionException(e);
			}
		}
		
	}
	
	@Test
	public void testLocalConnetion_whenServerEnabled() throws IOException {
		
//		Runtime.getRuntime().exec("cmd /c start D:\\DynamoDB\\runDynamoDB.bat");
		
		AmazonDynamoDBClient localClient = DataBaseConnection.getLocalClient();;
		try {
			ListTablesResult listTables = localClient.listTables();
			System.out.println(listTables.toString());
		} catch (AmazonClientException e) {
			throw new DynamoDBConnectionException(e);
		} finally{
			try {
//				localClient.shutdown();
			} catch (AmazonClientException e) {
				throw new DynamoDBConnectionException(e);
			}
		}
		
	}
	
	
	@Test
	public void testCloudConnetion_whenServerEnabled() {
		AmazonDynamoDBClient cloudClient = DataBaseConnection.getCloud_EuWest1_Client();
		try {
			ListTablesResult listTables = cloudClient.listTables();
			System.out.println(listTables.toString());
		} catch (AmazonClientException e) {
			throw new DynamoDBConnectionException(e);
		} finally{
			try {
//				cloudClient.shutdown();
			} catch (AmazonClientException e) {
				throw new DynamoDBConnectionException(e);
			}
		}
		
	}
	
	
	
	
	
	
}
