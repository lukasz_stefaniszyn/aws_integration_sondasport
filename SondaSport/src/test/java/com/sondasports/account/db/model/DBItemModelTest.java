package com.sondasports.account.db.model;

import static org.junit.Assert.*;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DBItemModelTest {

	@Test
	public void testConvertObjectToJson() throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		DBItemModel dbItemModel = new DBItemModel();
		
		//Convert Object into json 
		String itemJson = objectMapper.writeValueAsString(dbItemModel);
		
		String goldenItemJson = "{"
				+ "\"emailAddress\":\"\","
				+ "\"creationData\":\"1990.01.01\","
				+ "\"isActive\":true,"
				+ "\"tokenEmail\":\"zxcv123\","
				+ "\"tokenWasUsed\":false"
				+ "}";
		assertEquals("Generated json is not the same", goldenItemJson, itemJson);
	}
	
	
	@Test
	public void testConvertObjectToJsonFromMethod() throws JsonProcessingException {
		DBItemModel dbItemModel = new DBItemModel();
		
		//Convert Object into json 
		String itemJson = DBItemModel.convertDBItemObjetToJson(dbItemModel);
		
		String goldenItemJson = "{"
				+ "\"emailAddress\":\"\","
				+ "\"creationData\":\"1990.01.01\","
				+ "\"isActive\":true,"
				+ "\"tokenEmail\":\"zxcv123\","
				+ "\"tokenWasUsed\":false"
				+ "}";
		assertEquals("Generated json is not the same", goldenItemJson, itemJson);
	}
	
	@Test
	public void testConvertObjectToJsonFromCreatedObject() throws JsonProcessingException {
		DBItemModel dbItemModel = new DBItemModel();
		
		dbItemModel.setEmailAddress("hello@test.com");
		dbItemModel.setIsActive(true);
		dbItemModel.setCreationData("2010.02.02");
		dbItemModel.setTokenWasUsed(true);
		
		//Convert Object into json 
		String itemJson = dbItemModel.getJson();
		
		String goldenItemJson = "{"
				+ "\"emailAddress\":\"hello@test.com\","
				+ "\"creationData\":\"2010.02.02\","
				+ "\"isActive\":true,"
				+ "\"tokenEmail\":\"zxcv123\","
				+ "\"tokenWasUsed\":true"
				+ "}";
		assertEquals("Generated json is not the same", goldenItemJson, itemJson);
	}
	
	@Test
	public void testConvertJsonToObject() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		
		String itemJson = "{"
				+ "\"emailAddress\":\"\","
				+ "\"creationData\":\"1990.01.01\","
				+ "\"isActive\":false,"
				+ "\"tokenEmail\":\"\","
				+ "\"tokenWasUsed\":false"
				+ "}";

		//Convert Json to ObjectObject into json 
		DBItemModel dbItemModel = objectMapper.readValue(itemJson, DBItemModel.class);
		
		assertEquals("Address not the same", "", dbItemModel.getEmailAddress());
		assertEquals("CreationData not the same", "1990.01.01", dbItemModel.getCreationData());
		assertEquals("IsActive not the same", false, dbItemModel.getIsActive());
		assertEquals("Token not the same", "", dbItemModel.getToken());
		assertEquals("TokenWasUsed not the same", false, dbItemModel.getTokenWasUsed());
	}

	
	
	@Test
	public void testConvertJsonToObjectFromMethod() throws Exception {
		
		String itemJson = "{"
				+ "\"emailAddress\":\"\","
				+ "\"creationData\":\"1990.01.01\","
				+ "\"isActive\":false,"
				+ "\"tokenEmail\":\"\","
				+ "\"tokenWasUsed\":false"
				+ "}";

		//Convert Json to ObjectObject into json 
		DBItemModel dbItemModel = DBItemModel.convertJsonToDBItemObject(itemJson);
		
		assertEquals("Address not the same", "", dbItemModel.getEmailAddress());
		assertEquals("CreationData not the same", "1990.01.01", dbItemModel.getCreationData());
		assertEquals("IsActive not the same", false, dbItemModel.getIsActive());
		assertEquals("Token not the same", "", dbItemModel.getToken());
		assertEquals("TokenWasUsed not the same", false, dbItemModel.getTokenWasUsed());
	}

	
	
}
