package com.sondasports.account.lambda;

import static org.junit.Assert.*;

import java.io.IOException;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.hamcrest.CoreMatchers.*;
import org.hamcrest.core.IsEqual;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.lambda.runtime.Context;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.sondasports.account.db.DataBaseConnection;
import com.sondasports.account.db.model.DBItemModel;
import com.sondasports.account.db.settings.DynamoDbTableSettings;
import com.sondasports.account.db.utils.DynamoDBCreateTable;
import com.sondasports.account.lambda.core.DBitem;
import com.sondasports.account.lambda.core.RequestData;
import com.sondasports.account.lambda.core.ResponseData;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public class DBitemUpdateEmailLocalTest {
	
	static String TABLE_NAME = DynamoDbTableSettings.TABLE_NAME_ACCOUNT.toString();
	
	private static AmazonDynamoDBClient client;
	private static DynamoDB dynamoDB;
	
	private static Table table;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		client = DataBaseConnection.getLocalClient();
		dynamoDB = new DynamoDB(client);
		
		String csvFilePath = "dbItemTestData/dbItemsTestData_1_2.csv";
		table = DynamoDBCreateTable.createTable(dynamoDB, TABLE_NAME, csvFilePath);
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		DynamoDBCreateTable.deleteTable(dynamoDB, TABLE_NAME);
		// dynamoDB.shutdown();
	}
	
	@Before
	public void setUp() throws Exception {
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void test_updateIsActive_EmailExists() throws JsonProcessingException, IOException {
		
		String emailAddress = "10_test@softwareautomation.pl";
		Context context = null;
		
		// Set variables
		RequestData request;
		ResponseData dBItemModel;
		DBItemModel convertJsonToDBItemObject;
		boolean isActive;
		Boolean isActive_After;
		
		// Test updatig data
		isActive = false;
		UpdateItemOutcome updateDBItemIsActive = DBitem.updateDBItemIsActive(table, emailAddress, isActive);
		
		// Verify if 'isActive'value was updated
		request = new RequestData().setEmailAddress(emailAddress);
		dBItemModel = new DBitemGetEmail(client).handleRequest(request, context);
		convertJsonToDBItemObject = DBItemModel.convertJsonToDBItemObject(dBItemModel.getResponse());
		isActive_After = convertJsonToDBItemObject.getIsActive();
		
		assertThat("Item attribute 'isActive' was not updated", isActive, CoreMatchers.is(isActive_After));
		
		// Test updatig data
		isActive = true;
		updateDBItemIsActive = DBitem.updateDBItemIsActive(table, emailAddress, isActive);
		System.out.println("Update output: " + updateDBItemIsActive.toString());
		
		// Verify if 'isActive'value was updated
		request = new RequestData().setEmailAddress(emailAddress);
		dBItemModel = new DBitemGetEmail(client).handleRequest(request, context);
		convertJsonToDBItemObject = DBItemModel.convertJsonToDBItemObject(dBItemModel.getResponse());
		isActive_After = convertJsonToDBItemObject.getIsActive();
		
		assertThat("Item attribute 'isActive' was not updated", isActive, CoreMatchers.is(isActive_After));
		
	}
	
	@Test
	public void test_updateIsActive_EmailNotExists() throws JsonProcessingException, IOException {
		
		String emailAddress = "notExistingEmail@softwareautomation.pl";
		
		// Test updatig data
		boolean isActive = true;
		UpdateItemOutcome updateDBItemIsActive = DBitem.updateDBItemIsActive(table, emailAddress, isActive);
		System.out.println("Update output: " + updateDBItemIsActive.toString());
		assertThat(updateDBItemIsActive.toString(), CoreMatchers.equalTo("{}"));
		
	}
	
	@Test
	public void test_updateTokenWasUsed_EmailExists() throws JsonProcessingException, IOException {
		
		String emailAddress = "9_test@softwareautomation.pl";
		Context context = null;
		
		// Set variables
		RequestData request;
		ResponseData dBItemModel;
		DBItemModel convertJsonToDBItemObject;
		boolean tokenWasUsed;
		Boolean tokenWasUsed_After;
		
		// Test updatig data
		tokenWasUsed = false;
		UpdateItemOutcome updateDBItemIsActive = DBitem.updateDBItemTokenWasUsed(table, emailAddress, tokenWasUsed);
		
		// Verify if 'tokenWasUsed'value was updated
		request = new RequestData().setEmailAddress(emailAddress);
		dBItemModel = new DBitemGetEmail(client).handleRequest(request, context);
		convertJsonToDBItemObject = DBItemModel.convertJsonToDBItemObject(dBItemModel.getResponse());
		tokenWasUsed_After = convertJsonToDBItemObject.getTokenWasUsed();
		
		assertThat("Item attribute 'tokenWasUsed' was not updated", tokenWasUsed, CoreMatchers.is(tokenWasUsed_After));
		
		// Test updatig data
		tokenWasUsed = true;
		updateDBItemIsActive = DBitem.updateDBItemTokenWasUsed(table, emailAddress, tokenWasUsed);
		System.out.println("Update output: " + updateDBItemIsActive.toString());
		
		// Verify if 'tokenWasUsed'value was updated
		request = new RequestData().setEmailAddress(emailAddress);
		dBItemModel = new DBitemGetEmail(client).handleRequest(request, context);
		convertJsonToDBItemObject = DBItemModel.convertJsonToDBItemObject(dBItemModel.getResponse());
		tokenWasUsed_After = convertJsonToDBItemObject.getTokenWasUsed();
		
		assertThat("Item attribute 'tokenWasUsed' was not updated", tokenWasUsed, CoreMatchers.is(tokenWasUsed_After));
		
	}
	
	@Test
	public void test_updateTokenWasUsed_EmailNotExists() throws JsonProcessingException, IOException {
		
		String emailAddress = "notExistingEmail@softwareautomation.pl";
		
		// Test updatig data
		boolean tokenWasUsed = true;
		UpdateItemOutcome updateDBItemIsActive = DBitem.updateDBItemIsActive(table, emailAddress, tokenWasUsed);
		System.out.println("Update output: " + updateDBItemIsActive.toString());
		assertThat(updateDBItemIsActive.toString(), CoreMatchers.equalTo("{}"));
		
	}
	
}
