package com.sondasports.account.lambda;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.lambda.runtime.Context;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.sondasports.account.db.DataBaseConnection;
import com.sondasports.account.db.model.DBItemModel;
import com.sondasports.account.db.settings.DynamoDbTableSettings;
import com.sondasports.account.db.utils.DynamoDBCreateTable;
import com.sondasports.account.lambda.core.DBitem;
import com.sondasports.account.lambda.core.RequestData;
import com.sondasports.account.lambda.core.ResponseData;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 */
public class DBitemActivateTokenTest {
	
	static String TABLE_NAME = DynamoDbTableSettings.TABLE_NAME_ACCOUNT.toString();
	
	private static AmazonDynamoDBClient client;
	private static DynamoDB dynamoDB;
	
	private static Table table;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		client = DataBaseConnection.getLocalClient();
		dynamoDB = new DynamoDB(client);
		
		String csvFilePath = "dbItemTestData/dbItemsTestData_1_2.csv";
		table = DynamoDBCreateTable.createTable(dynamoDB, TABLE_NAME, csvFilePath);
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		DynamoDBCreateTable.deleteTable(dynamoDB, TABLE_NAME);
		// dynamoDB.shutdown();
	}
	
	@Before
	public void setUp() throws Exception {
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void test_tokenNotExist() throws JsonProcessingException, IOException {
		
		String token = "NotExistingToken";
		Context context = new TestContext();
		
		RequestData request = new RequestData().setTokenEmail(token);
		
		ResponseData responseData = new DBitemActivateToken(client).handleRequest(request, context);
		String itemJson = responseData.toString();
		
		String goldenJson = "Given item: (" + token + ") was not found in table: (" + TABLE_NAME + ").";
		
		assertThat("Item was found, but should not", itemJson, CoreMatchers.is(goldenJson));
		
		
	}
	
	@Test
	public void test_tokenWasUsedAlreadyUsed_TokenEmailExists()
			throws JsonParseException, JsonMappingException, IOException {
		
		String token = "he236eoqbir886cf0in0mdodkb";
		Context context = null;
		
		// Set variables
		RequestData request;
		ResponseData responseData;
		boolean tokenWasUsed;
		UpdateItemOutcome updateDBItemIsActive;
		
		// Get email instance based on Token
		DBItemModel dbItemModel = DBitem.getDBItemEmailByToken(table, token);
		String emailAddress = dbItemModel.getEmailAddress();
		
		// Set 'wasTokenUsed' to true
		tokenWasUsed = true;
		updateDBItemIsActive = DBitem.updateDBItemTokenWasUsed(table, emailAddress, tokenWasUsed);
		
		// Verify ResponseData when email attribute 'wasTokenUsed' was set to true
		request = new RequestData().setTokenEmail(token);
		responseData = new DBitemActivateToken(client).handleRequest(request, context);
		
		assertThat("Token request have wrong response message when 'wasTokenUsed'== true", responseData.getResponse(),
				CoreMatchers.is("Token (" + token + ") already used"));
		
	}
	
	@Test
	public void test_tokenWasNotUsed_ActivateAccount_TokenEmailExists() throws JsonProcessingException, IOException {
		
		String token = "he236eoqbir886cf0in0mdodkb";
		Context context = null;
		
		// Set variables
		RequestData request;
		ResponseData responseData;
		boolean tokenWasUsed;
		UpdateItemOutcome updateDBItemIsActive;
		
		// Get email instance based on Token
		DBItemModel dbItemModel = DBitem.getDBItemEmailByToken(table, token);
		String emailAddress = dbItemModel.getEmailAddress();
		Boolean isActive = dbItemModel.getIsActive();
		
		// Set 'wasTokenUsed' to false
		tokenWasUsed = false;
		updateDBItemIsActive = DBitem.updateDBItemTokenWasUsed(table, emailAddress, tokenWasUsed);
		
		// Set 'isActive' to false
		isActive = false;
		updateDBItemIsActive = DBitem.updateDBItemIsActive(table, emailAddress, isActive);
		
		// Verify email attribute 'isAcive' should be set to true
		request = new RequestData().setTokenEmail(token);
		responseData = new DBitemActivateToken(client).handleRequest(request, context);
		System.out.println("Response: " + responseData);
		assertThat("Token request have wrong response message when 'wasTokenUsed'== false and 'isActive'== false",
				responseData.getResponse(), CoreMatchers.is("Email account (" + emailAddress + ") is now active."));
		
		// Verify email attribute 'wasTokenUsed' should be set to true
		dbItemModel = DBitem.getDBItemEmail(table, emailAddress);
		tokenWasUsed = dbItemModel.getTokenWasUsed();
		assertThat("Attribute value 'wasTokenUsed' was not set to True", tokenWasUsed, CoreMatchers.is(true));
	}
	
}
