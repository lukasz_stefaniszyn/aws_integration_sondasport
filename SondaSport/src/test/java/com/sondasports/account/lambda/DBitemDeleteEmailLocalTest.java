package com.sondasports.account.lambda;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.mail.Message;
import javax.mail.MessagingException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.sondasports.account.apigateway.settings.ApiGatewayUrl;
import com.sondasports.account.db.DataBaseConnection;
import com.sondasports.account.db.model.AttributeNaming;
import com.sondasports.account.db.model.DBItemModel;
import com.sondasports.account.db.settings.DynamoDbTableSettings;
import com.sondasports.account.db.utils.DynamoDBCreateTable;
import com.sondasports.account.db.utils.GenerateToken;
import com.sondasports.account.lambda.core.DBitem;
import com.sondasports.account.lambda.core.RequestData;
import com.sondasports.account.lambda.core.ResponseData;
import com.sondasports.account.ses.SimpleEmailSenderTest;
import com.sondasports.account.ses.mailClient.EmailClientInbox;
import com.sondasports.account.ses.settings.MailSettings;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 */
public class DBitemDeleteEmailLocalTest {
	
	static String TABLE_NAME = DynamoDbTableSettings.TABLE_NAME_ACCOUNT.toString();
	
	private static AmazonDynamoDBClient clientDynamoDB;
	private static DynamoDB dynamoDB;
	
	private static Table table;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
//		clientDynamoDB = DataBaseConnection.getLocalClient();
		clientDynamoDB = DataBaseConnection.getCloud_EuWest1_Client();
		dynamoDB = new DynamoDB(clientDynamoDB);
		
		String csvFilePath = "dbItemTestData/dbItemsTestData_1_2.csv";
		table = DynamoDBCreateTable.createTable(dynamoDB, TABLE_NAME, csvFilePath);
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		DynamoDBCreateTable.deleteTable(dynamoDB, TABLE_NAME);
		// client.shutdown();
	}
	
	@Before
	public void setUp() throws Exception {
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void test_VerifyIsActive_EmailInDB() throws JsonParseException, JsonMappingException, IOException {
		
		String emailAddress = "0_test@softwareautomation.pl";
		Context context = new TestContext();
		
		// Set variables
		RequestData request;
		ResponseData responseData;
		Boolean isActive;
		
		// Execute DeleteEmail process
		request = new RequestData().setEmailAddress(emailAddress);
		responseData = new DBitemDeleteEmail(clientDynamoDB).handleRequest(request, context);
		
		// Verify Email attribute 'isActive' was set to false
		DBItemModel dbItemEmail = DBitem.getDBItemEmail(table, emailAddress);
		isActive = dbItemEmail.getIsActive();
		
		assertThat("Attibute 'isActive' value was not updated", isActive, is(false));
		
	}
	
	@Test
	public void test_VerifyNewToken_EmailInDB() throws JsonParseException, JsonMappingException, IOException {
		
		String emailAddress = "1_test@softwareautomation.pl";
		Context context = new TestContext();
		
		// Set variables
		RequestData request;
		ResponseData responseData;
		
		// Get current 'tokenEmail' value
		DBItemModel dbItemEmail = DBitem.getDBItemEmail(table, emailAddress);
		String token = dbItemEmail.getToken();
		
		// Execute DeleteEmail process
		request = new RequestData().setEmailAddress(emailAddress);
		responseData = new DBitemDeleteEmail(clientDynamoDB).handleRequest(request, context);
		
		// Verify TokenEmail attribute was changed
		dbItemEmail = DBitem.getDBItemEmail(table, emailAddress);
		String newToken = dbItemEmail.getToken();
		assertThat(newToken, is(not(equalTo(GenerateToken.EMPTY_TOKEN_STRING))));
		
		assertThat("Attibute 'tokenEmail' value was not updated", newToken, is(not(token)));
	}
	
	@Test
	public void test_VerifyEmailSentAndRecieved_EmailInDB()
			throws JsonParseException, JsonMappingException, IOException, InterruptedException, MessagingException {
		
		String emailAddress = "2_test@softwareautomation.pl";
		Context context = new TestContext();
		
		// Set variables
		RequestData request;
		ResponseData responseData;
		
		// Set values to verify if email was recived
		final Date whenEmailWasSent = new Date();
		TimeUnit.SECONDS.sleep(1);
		
		// Execute DeleteEmail process
		request = new RequestData().setEmailAddress(emailAddress);
		responseData = new DBitemDeleteEmail(clientDynamoDB).handleRequest(request, context);
		
		// Verify that email was sent and recieved
		String subject = MailSettings.SUBJECT.toString();
		
		if (false == emailWasRecieved(emailAddress, subject, whenEmailWasSent)) {
			fail("Timeout in waiting for email to recieve ");
		}
		
		//Verify message output response
		assertThat(responseData.toString(), containsString("Email (" + emailAddress + ") account is set to inactive. Reactivation token was sent."));
		
		
//		
//		EmailClientInbox emailClientInbox = new EmailClientInbox(emailAddress);
//		Message latestMail = emailClientInbox.getLatestMail();
//		String rcvBody = latestMail.getContent().toString();
		
		
		
		
	}
	
	@Test
	public void test_VerifyEmailBody_EmailInDB()
			throws JsonParseException, JsonMappingException, IOException, InterruptedException, MessagingException {
		
		String emailAddress = "3_test@softwareautomation.pl";
		Context context = new TestContext();
		
		// Set variables
		RequestData request;
		ResponseData responseData;
		
		// Set values to verify if email was recived
		final Date whenEmailWasSent = new Date();
		TimeUnit.SECONDS.sleep(1);
		
		// Execute DeleteEmail process
		request = new RequestData().setEmailAddress(emailAddress);
		responseData = new DBitemDeleteEmail(clientDynamoDB).handleRequest(request, context);
		
		// Verify that email was sent and received
		String subject = MailSettings.SUBJECT.toString();
		
		if (false == emailWasRecieved(emailAddress, subject, whenEmailWasSent)) {
			fail("Timeout in waiting for email to recieve ");
		}
		EmailClientInbox emailClientInbox = new EmailClientInbox(emailAddress);
		Message latestMail = emailClientInbox.getLatestMail();
		String rcvBody = latestMail.getContent().toString();
		
		
		String body = "Dear user,\n" + "Thank You for being with us\n"
				+ "We have recieved Your resignation request for email account (${emailAddress})\n"
				+ "\n"
				+ "Your account is now disabled.\n"
				+ "You can always reactivate it by clicking this link: \n"
				+ "${tokenUrl}";
		
		body = body.replace("${emailAddress}", emailAddress).replace("${tokenUrl}", generateTokenUrl(""));
		
		System.out.println("rcvBody.contains(body):   " + rcvBody.contains(body));
//		equalToIgnoringWhiteSpace
		
		assertThat("Recieved mail body is diffrent than excpected", replaceWhiteSpaces(rcvBody) , containsString(replaceWhiteSpaces(body)));
		
	}
	
	private String replaceWhiteSpaces(String text) {
		text =  text.replaceAll("\\s+", "");	
		return text;
	}

	@Test
	public void test_EmailWasDeactivated() throws JsonParseException, JsonMappingException, IOException {
		
		String emailAddress = "6_test@softwareautomation.pl";
		Context context = new TestContext();
		
		// Get email address
		DBItemModel dbItemEmail = DBitem.getDBItemEmail(table, emailAddress);
		assertThat(dbItemEmail.getIsActive(), is(false));
		
		// Delete not existing email
		RequestData request = new RequestData().setEmailAddress(emailAddress);
		ResponseData handleRequest = new DBitemDeleteEmail(clientDynamoDB).handleRequest(request, context);
		
		assertThat(handleRequest.toString(), containsString("Email (" + emailAddress + ")  account is not active. Please contact with adminitrator"));
	}
	

	
	
	@Test
	public void test_EmailNotInDB() throws JsonParseException, JsonMappingException, IOException {
		
		String emailAddress = "emailNotExisting@mail.com";
		Context context = new TestContext();
		
		// Get email address
		DBItemModel dbItemEmail = DBitem.getDBItemEmail(table, emailAddress);
		assertThat(dbItemEmail, nullValue());
		
		// Delete not existing email
		RequestData request = new RequestData().setEmailAddress(emailAddress);
		ResponseData handleRequest = new DBitemDeleteEmail(clientDynamoDB).handleRequest(request, context);
		
		assertThat(handleRequest.toString(), containsString("Email (" + emailAddress + ") was not found in table (" + TABLE_NAME + ")."));
		
	}
	
	private boolean emailWasRecieved(String emailAddress, String subject, Date whenEmailWasSent)
			throws MessagingException, InterruptedException {
		
		boolean checkLastMail = true;
		String info = "Waiting for mail";
		
		EmailClientInbox emailClientInbox = null;
		while (checkLastMail) {
			
			if (SimpleEmailSenderTest.isTimeout(whenEmailWasSent)) {
				checkLastMail = false;
				emailClientInbox.close();
				return false;
			}
			
			emailClientInbox = new EmailClientInbox(emailAddress);
			Message latestMail = emailClientInbox.getLatestMail();
			
			if (null == latestMail) {
				TimeUnit.SECONDS.sleep(2);
				continue;
			}
			Date sentDate = latestMail.getSentDate();
			
			info = info.concat(". ");
			System.out.println(info);
			
			if ((whenEmailWasSent.getTime() <= sentDate.getTime()) && (latestMail.getSubject().equals(subject))) {
				checkLastMail = false;
				emailClientInbox.close();
				return true;
				
			}
			TimeUnit.SECONDS.sleep(2);
			
		}
		
		try {
			emailClientInbox.close();
		} catch (NullPointerException e) { // NOP
		
		}
		return true;
	}
	
	private String generateTokenUrl(String newToken) {
		List<String> list = Arrays.asList(ApiGatewayUrl.ENDPOINT.toString(), ApiGatewayUrl.STAGE_DEV.toString(),
				ApiGatewayUrl.RESOURCE_URL.toString());
		String tokenUrl = String.join("/", list).concat("?"+ AttributeNaming.tokenName + "=" + newToken); // Generate activation token url
		System.out.println("TOKENURL:" + tokenUrl);
		return tokenUrl;

		
		
		
		
	}
}
