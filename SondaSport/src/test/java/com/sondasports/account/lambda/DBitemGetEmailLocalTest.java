package com.sondasports.account.lambda;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.hamcrest.CoreMatchers.*;
import org.hamcrest.core.IsEqual;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.sondasports.account.db.DataBaseConnection;
import com.sondasports.account.db.settings.DynamoDbTableSettings;
import com.sondasports.account.db.utils.DynamoDBCreateTable;
import com.sondasports.account.lambda.core.RequestData;
import com.sondasports.account.lambda.core.ResponseData;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public class DBitemGetEmailLocalTest {

	static String TABLE_NAME = DynamoDbTableSettings.TABLE_NAME_ACCOUNT.toString();

	private static AmazonDynamoDBClient client;
	private static DynamoDB dynamoDB;

	private static Table table;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		client = DataBaseConnection.getLocalClient();
		dynamoDB = new DynamoDB(client);
		
		String csvFilePath = "dbItemTestData/dbItemsTestData_1_2.csv";
		table = DynamoDBCreateTable.createTable(dynamoDB, TABLE_NAME, csvFilePath);

		
		

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		DynamoDBCreateTable.deleteTable(dynamoDB, TABLE_NAME);
//		dynamoDB.shutdown();
	}
	
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test_GetExistingEmail() throws JsonProcessingException, IOException {
		
		
		String emailAddress = "10_test@softwareautomation.pl"; 
		Context context = new TestContext();
		
		
		
		RequestData request = new RequestData()
				.setEmailAddress(emailAddress);
			

		
		ResponseData dBItemModel = new DBitemGetEmail(client).handleRequest(request , context);

		String itemJson = dBItemModel.toString();
		System.out.println("itemJson:" + itemJson);
		String goldenJson = "{"
				+ "\"emailAddress\":\"10_test@softwareautomation.pl\","
				+ "\"creationData\":\"2016.06.11\","
				+ "\"isActive\":false,"
				+ "\"tokenEmail\":\"he236eoqbir886cf0in0mdodkb\","
				+ "\"tokenWasUsed\":true"
				+ "}";
		
		assertThat("Data taken from DB has different format", goldenJson , CoreMatchers.is(itemJson));
		
		
//		int[] test = new int[20] ;
//		for (int i : test) {
//			 SecureRandom random = new SecureRandom();
//			 String token = new BigInteger(130, random).toString(32);
//			 System.out.println(token);
		}
	
	
	
	@Test
	public void test_GetNotExistingEmail() throws JsonProcessingException, IOException {
		
		
		String emailAddress = "emailNotExisting@mail.com"; 
		Context context = new TestContext();
		
		
		
		RequestData request = new RequestData()
				.setEmailAddress(emailAddress);
			

		
		ResponseData dBItemModel = new DBitemGetEmail(client).handleRequest(request , context);
		String itemJson = dBItemModel.toString();

		
		String goldenJson = "Given item: ("+ emailAddress +") was not found in table: ("+ TABLE_NAME +").";
		
		assertThat("Item was found, but should not", goldenJson , CoreMatchers.is(itemJson));
		
		
//		int[] test = new int[20] ;
//		for (int i : test) {
//			 SecureRandom random = new SecureRandom();
//			 String token = new BigInteger(130, random).toString(32);
//			 System.out.println(token);
			 

		}

	
	@Test
	public void test_EmailWithEmptyString() {
		
		
//		{
//			  "emailAddress" : "",
//			  "tokenEmail" : "h7dk9hb9vfuedt3n1e9hebeeph"
//			}
//		
		
		String emailAddress = ""; 
		Context context = new TestContext();
		
		
		
		RequestData request = new RequestData()
				.setEmailAddress(emailAddress);
			

		
		ResponseData dBItemModel = new DBitemGetEmail(client).handleRequest(request , context);
		String itemJson = dBItemModel.toString();

		
		String goldenJson = "Neither email nor token were used in request";
		
		assertThat("Item was found, but should not", goldenJson , CoreMatchers.is(itemJson));
		
		
	
	}
	

}
