/**
 * 
 */
package com.sondasports.account.ses;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.mail.Message;
import javax.mail.MessagingException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.sondasports.account.db.DataBaseConnection;
import com.sondasports.account.ses.mailClient.EmailClientInbox;
import com.sondasports.account.ses.settings.MailSettings;
import com.sondasports.account.ses.settings.SesConnection;
import com.sondasports.credentials.GetAWSCredentials;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public class SimpleEmailSenderTest {
	
	
	private static AmazonSimpleEmailServiceClient client;
	private static SimpleEmailSender simpleEmailSender;
	private static EmailClientInbox emailClientInbox;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		client = SesConnection.getCloud_EuWest1_Client();
		simpleEmailSender = new SimpleEmailSender(client);
		
		emailClientInbox = new EmailClientInbox();
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		try{
			emailClientInbox.close();
		} catch (Exception e){
			//NOP
		}
	}
	
	@Before
	public void setUp() throws Exception {
	}
	
	@After
	public void tearDown() throws Exception {
		TimeUnit.SECONDS.sleep(1); //Due to SES limitation 1mail/sec
	}
	
	@Test
	public void test() throws MessagingException, InterruptedException {
		
		
		String From = MailSettings.FROM.toString();
		String Subject = MailSettings.SUBJECT.toString();

		String To = new GetAWSCredentials("sondasport_test_email").getCredentials().getAWSAccessKeyId(); 
		//String To = "test2@softwareautomation.pl"; // Replace with a "To" address. If you have not yet requested
		String Body = "This email was sent through Amazon SES by using the AWS SDK for Java.";
		
		
		final Date whenEmailWasSent = new Date();
		TimeUnit.SECONDS.sleep(1);
		simpleEmailSender.sendMail(To, Body);
		
		//Verify email was recieved
		
		if (false == emailWasRecieved(Subject, whenEmailWasSent)){
			fail("Timeout in waiting for email to recieve ");
		};
	}

	public boolean emailWasRecieved(String subject, final Date whenEmailWasSent)
			throws MessagingException, InterruptedException {
		boolean checkLastMail = true;
		String info = "Waiting for mail";

		while (checkLastMail ){
			if (SimpleEmailSenderTest.isTimeout(whenEmailWasSent)) {
				checkLastMail = false;
				emailClientInbox.close();
				return false;
			}
			
			emailClientInbox = new EmailClientInbox();
			Message latestMail = emailClientInbox.getLatestMail();
			
			if (null == latestMail){
				TimeUnit.SECONDS.sleep(2);
				continue;
			}
			Date sentDate = latestMail.getSentDate();
			
			info = info.concat(". ");
			System.out.println(info);
			
			if ((whenEmailWasSent.getTime() <= sentDate.getTime()) && (latestMail.getSubject().equals(subject))) {
				checkLastMail = false;
				emailClientInbox.close();
				return true;
				
			}
			TimeUnit.SECONDS.sleep(2);
			
				
		}
		return true;
	}

	public static boolean isTimeout(final Date whenEmailWasSent) {
		return new Date().getSeconds() - whenEmailWasSent.getSeconds() >= TimeUnit.SECONDS.toSeconds(60);
	}
	
	@Ignore
	@Test
	public void test_2() {
		
		String To = "test2@softwareautomation.pl"; 
		String Body = "This email was sent through Amazon SES by using the AWS SDK for Java.";
		
		simpleEmailSender.sendMail(To, Body);
	}
	
}
