package com.sondasports.account.ses.mailClient;

import java.io.IOException;
/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
import java.util.Properties;

import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;

import com.sondasports.credentials.GetAWSCredentials;

public class EmailClientInbox {
	
	private Folder emailFolder;
	private Store store;
	
	public EmailClientInbox() {
		//
		// String host = "softwareautomation.pl";// change accordingly
		//
		// String profile = "sondasport_test_email";
		// AWSCredentials credentials = new GetAWSCredentials(profile).getCredentials();
		// String username = credentials.getAWSAccessKeyId(); // this is email address 'test@mydomain.com'
		// String password = credentials.getAWSSecretKey();// this is password 'ThisIsPassword'
		//
		this("softwareautomation.pl",
				new GetAWSCredentials("sondasport_test_email").getCredentials().getAWSAccessKeyId(),
				new GetAWSCredentials("sondasport_test_email").getCredentials().getAWSSecretKey());
	}
	
	public EmailClientInbox(String username) {
		this("softwareautomation.pl",
				username,
				new GetAWSCredentials("sondasport_test_email").getCredentials().getAWSSecretKey());
	}
	
	public EmailClientInbox(String host, String username, String password) {
		
		// create properties field
		Properties properties = new Properties();
		
		properties.put("softwareautomation.pl", host);
		properties.put("softwareautomation.pl", "995");
		properties.put("mail.pop3.starttls.enable", "true");
		Session emailSession = Session.getDefaultInstance(properties);
		
		try {
			// create the POP3 store object and connect with the pop server
			this.store = emailSession.getStore("pop3s");
			
			store.connect(host, username, password);
			
			// create the folder object and open it
			this.emailFolder = store.getFolder("INBOX");
			emailFolder.open(Folder.READ_ONLY);
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		this.close();
	}
	
	public void close() {
		// close the store and folder objects
		try {
			this.emailFolder.close(false);
			this.store.close();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public Message getLatestMail() {
		// retrieve the messages from the folder in an array and print it
		
		
		/*  Get the messages which is unread in the Inbox*/
		 Message messages[] = null;
		try {
			messages = this.emailFolder.search(new FlagTerm(new Flags(Flag.RECENT), false));
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 /* Use a suitable FetchProfile    */
		 FetchProfile fp = new FetchProfile();
		 fp.add(FetchProfile.Item.ENVELOPE);
		 fp.add(FetchProfile.Item.CONTENT_INFO);
		 try {
			this.emailFolder.fetch(messages, fp);
		} catch (MessagingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		 if (0 == messages.length){
			 return null;
		 }
		 Message message = messages[messages.length - 1];
			return message;
	}
	
	public void getAllMails() {
		
		// retrieve the messages from the folder in an array and print it
		Message[] messages = null;
		try {
			messages = emailFolder.getMessages();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		System.out.println("messages.length---" + messages.length);
		
		for (int i = 0, n = messages.length; i < n; i++) {
			Message message = messages[i];
			System.out.println("---------------------------------");
			System.out.println("Email Number " + (i + 1));
			try {
				System.out.println("Subject: " + message.getSubject());
				System.out.println("From: " + message.getFrom()[0]);
				System.out.println("Text: " + message.getContent().toString());
			} catch (MessagingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
	public static void main(String[] args) throws MessagingException {
		
		EmailClientInbox checkingMails = new EmailClientInbox();
		checkingMails.getAllMails();
		
		System.out.println("----------------------------------------------------------");
		
		Message latestMail = checkingMails.getLatestMail();
		System.out.println("Sent: " + latestMail.getSentDate().toString());
		
		checkingMails.close();
		
	}
	
}