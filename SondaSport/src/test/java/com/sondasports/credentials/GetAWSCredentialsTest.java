package com.sondasports.credentials;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;
import org.junit.rules.ExpectedException;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;

public class GetAWSCredentialsTest {

	
	
	@Test
	public void testGetAWSCerdentials_NoParams() {
		
		AWSCredentials credentials = new GetAWSCredentials().getCredentials();
		
		String goldenAWSAccessKeyID = "testaccessKey";
		String goldenAWSSecretKey = "testsecretKey";
		assertTrue("Tested AWSAccessKeyId is not the same as in credentials file", credentials.getAWSAccessKeyId().equals(goldenAWSAccessKeyID ));
		assertTrue("Tested AWSSecretKey is not the same as in credentials file", credentials.getAWSSecretKey().equals(goldenAWSSecretKey));
	}
	
	@Test
	public void testGetAWsCredentials_ProfileFolder() throws Exception {
		
		String profile = "sondasport_test3";
		AWSCredentials credentials = new GetAWSCredentials(profile).getCredentials();
		
		
		String goldenAWSAccessKeyID = "asdzxcXBSFSI7BK2ZGTYGEJ4KLB";
		String goldenAWSSecretKey = "CadadAZS....n5jFXjsxB1";
		assertTrue("Tested AWSAccessKeyId is not the same as in credentials file", credentials.getAWSAccessKeyId().equals(goldenAWSAccessKeyID ));
		assertTrue("Tested AWSSecretKey is not the same as in credentials file", credentials.getAWSSecretKey().equals(goldenAWSSecretKey));
		
		
	}
	
	

	@Test
	public void shouldTestExceptionMessage() throws IndexOutOfBoundsException {
	    List<Object> list = new ArrayList<Object>();

	    thrown.expect(IndexOutOfBoundsException.class);
	    thrown.expectMessage("Index: 0, Size: 0");
	    list.get(0); // execution will never get past this line
	}
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	@SuppressWarnings("deprecation")
	@Test
	public void testGetAWsCredentials_MissingProfile() throws AmazonClientException {
		String profile = "missing_profile";
		
		thrown.expect(AmazonClientException.class);
		thrown.expectMessage(CoreMatchers.containsString("Cannot load profile " + profile + " from the credential profiles file. Please make sure that your credentials file is at the correct location: "));

		AWSCredentials credentials = new GetAWSCredentials(profile).getCredentials();
		
//		String profilesConfigFilePath = System.getProperty("user.home") + "/.aws/credentials";
//		"Cannot load the credentials from the credential profiles file. " +
//        "Please make sure that your credentials file is at the correct " +
//        "location: " + profilesConfigFilePath + ", and is in valid format.",
//		JUnitMatchers.containsString("Size: 0")
	}
	

	@Test
	public void testGetAWsCredentials_ProfileFolder_and_Profile() throws Exception {
		File credentialFile= new File(this.getClass().getResource("/dbConnection/credentials.txt").getFile());
		String profilesConfigFilePath = credentialFile.getAbsolutePath();
		
		String profile = "sondasport_test2";
		AWSCredentials credentials = new GetAWSCredentials(profilesConfigFilePath, profile).getCredentials();
		
		
		String goldenAWSAccessKeyID = "123123XBSFSI7BK2ZGTYGEJ4KLB";
		String goldenAWSSecretKey = "CAZS.czxxzz213n5jFXjsxB1";
		assertTrue("Tested AWSAccessKeyId is not the same as in credentials file", credentials.getAWSAccessKeyId().equals(goldenAWSAccessKeyID ));
		assertTrue("Tested AWSSecretKey is not the same as in credentials file", credentials.getAWSSecretKey().equals(goldenAWSSecretKey));
		
	}
	
	

}
