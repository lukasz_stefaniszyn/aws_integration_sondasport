package com.sondasports.credentials;

import java.io.File;
import java.io.IOException;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public class GetAWSCredentials {

	private static String profile = "default";
	private static String profilesConfigFilePath = System.getProperty("user.home") + "/.aws/credentials";

	private AWSCredentials credentials = null;

    public AWSCredentials getCredentials() {
		return credentials;
	}
	
	public GetAWSCredentials(){
		this(getProfilesConfigFilePath(), getProfile());
	}
	
	public GetAWSCredentials(String profile) {
		this(getProfilesConfigFilePath(), profile);
	}
	
	public GetAWSCredentials(String profilesConfigFilePath, String profile) {
		this.credentials = generateAWSCredentails(profilesConfigFilePath, profile);
	}
	
	private static String getProfilesConfigFilePath() {
		File file = new File(profilesConfigFilePath);
		return file.getAbsolutePath();
	}
	
	private static String getProfile() {
		return profile;
	}

	
	private AWSCredentials generateAWSCredentails(String profilesConfigFilePath, String profile) {
		AWSCredentials credentials = null;
//    	System.out.println("Verifying aws credentials in file: " +
//    			profilesConfigFilePath +
//    			" and with profile: "+
//    			profile);

        /*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (~/.aws/credentials).
         *
         * TransferManager manages a pool of threads, so we create a
         * single instance and share it throughout our application.
         */
        try {
            credentials = new ProfileCredentialsProvider(
            		profilesConfigFilePath,
            		profile)
        			.getCredentials();
        } catch (Exception e) {
        	throw new AmazonClientException(
                    "Cannot load profile " + profile + " from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location: " + profilesConfigFilePath + ", and is in valid format.",
                    e);
        }
		return credentials;
	}
		
	
	
	

}
