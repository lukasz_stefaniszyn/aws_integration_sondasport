/**
 * 
 */
package com.sondasports.account.lambda.utils;

import java.io.IOException;

import com.sondasports.account.lambda.core.RequestData;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 */
public class LambdaUtils {
	
	public static String generateExceptionText(String TableName, String requestAttributeName, IOException e) {
		String message = "Unable get from Table: (" + TableName + ") item: (" + requestAttributeName + "). Error: "
				+ e.getStackTrace().toString();
		System.out.println(message);
		return message;
	}
}
