package com.sondasports.account.lambda.core;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sondasports.account.db.model.AttributeNaming;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ AttributeNaming.emailAddressName, AttributeNaming.tokenName })
public class RequestData {
	
	@JsonProperty(AttributeNaming.emailAddressName)
	private String emailAddress = null;
	
	@JsonProperty(AttributeNaming.tokenName)
	private String tokenEmail = null;
	
	
	public RequestData() {
		
	}
	
	public RequestData(String emailAddress, String tokenEmail) {
		this.emailAddress = emailAddress;
		this.tokenEmail = tokenEmail;
	}
	
	
	
	@JsonIgnore
	public RequestData with(){
		return this;
	}
	
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty(AttributeNaming.emailAddressName)
	public String getEmailAddress() {
		return emailAddress;
	}
	
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	@JsonProperty(AttributeNaming.tokenName)
	public String getTokenEmail() {
		return tokenEmail;
	}

	
	public RequestData setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
		return this.with();
	}
	
	
	public RequestData setTokenEmail(String token) {
		this.tokenEmail = token;
		return this.with();
	}
	
	@Override
	public String toString() {
		return "Request: " +AttributeNaming.emailAddressName +  ": " + this.getEmailAddress() + " " + AttributeNaming.tokenName +": " + this.getTokenEmail();
	}

}