package com.sondasports.account.lambda.core;

import java.io.IOException;
import java.util.Iterator;

import com.amazonaws.services.dynamodbv2.document.AttributeUpdate;
import com.amazonaws.services.dynamodbv2.document.DeleteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Index;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.sondasports.account.db.model.AttributeNaming;
import com.sondasports.account.db.model.DBItemModel;
import com.sondasports.account.db.settings.DynamoDbTableSettings;
import com.sondasports.account.db.utils.DynamoDBUtils;
import com.sondasports.account.lambda.exceptions.TableGetException;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public class DBitem {

	
	
	public static Table getTableObject(DynamoDB dynamoDB, String tableName) throws TableGetException {
		Table table;
		try {
			table = DBitem.getTable(tableName, dynamoDB);
		} catch (InterruptedException e) {
			String message = "Table was found, but it is not active: " + e.getStackTrace().toString();
			System.err.println(message);
			throw new TableGetException(message);
			
		}

		if (!wasTableFound(table)) {
			String message = "Table was not found";
			System.err.println(message);
			throw new TableGetException(message);
		}

		
		System.out.println("Table was found:" + table.getTableName());
		return table;
	}

	
	
	public static DBItemModel getDBItemEmail(Table table, String emailAddress)
			throws JsonParseException, JsonMappingException, IOException {

		System.out.println("Getting item form table, based on: " + emailAddress);
		
		Item item = table.getItem(DBItemModel.HASH_KEY_NAME, emailAddress);
		if (item == null) {
			return null;
		}
		DBItemModel dbItemModel = DBItemModel.convertJsonToDBItemObject(item.toJSON());
		return dbItemModel;
	}

	/**
	 * @param table
	 * @param token
	 * @return null or founded DBItemModel 
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static DBItemModel getDBItemEmailByToken(Table table, String token)
			throws JsonParseException, JsonMappingException, IOException {

		System.out.println("Getting item form table, based on: " + token);
		
		String indexName = DBItemModel.GLOBAL_HASH_KEY_NAME;

		ItemCollection<QueryOutcome> items = getItemsBasedOnAttributeIndexName(table, token, indexName);
		if (items == null) {
			return null;
		}
		
		
		//TODO: This is return first from founded items. Consider using Paging with value 1
		DBItemModel dbItemModel = null;
		for (Item item : items) {
			dbItemModel = DBItemModel.convertJsonToDBItemObject(item.toJSON());
			break;
		}
		return dbItemModel;
	}


	public static DeleteItemOutcome delDBItemEmail(Table table, String emailAddress) {

		DeleteItemOutcome deleteItemResult = table.deleteItem(DBItemModel.HASH_KEY_NAME, emailAddress);

		return deleteItemResult;
	}

	public static UpdateItemOutcome updateDBItemIsActive(Table table, String emailAddress, Boolean value) {
		return updateValue(table, AttributeNaming.isActiveName, emailAddress, value);
	}

	public static UpdateItemOutcome updateDBItemTokenWasUsed(Table table, String emailAddress, Boolean value) {
		return updateValue(table, AttributeNaming.tokenWasUsedName, emailAddress, value);
	}
	

	public static UpdateItemOutcome updateDBItemToken(Table table, String emailAddress, String value) {
		return updateValue(table, AttributeNaming.tokenName, emailAddress, value);
		
	}

	private static UpdateItemOutcome updateValue(Table table, String attributeName, String emailAddress,
			Object value) {
		System.out.println("For emailAddress (" + emailAddress + ") updating attribute (" + attributeName + ") into value (" + value.toString() + ").");
		UpdateItemOutcome updateItem = table.updateItem(DBItemModel.HASH_KEY_NAME, emailAddress,
				new AttributeUpdate(attributeName).put(value));
		return updateItem;
	}

	
	/**
	 * Query data base and return list of all items where AttributeIndexName equals Value 
	 * @param table - on which table make query
	 * @param value  -  value to search 
	 * @param indexName - on which Attribute/Column name make search
	 * @return null or Collection of founded items 
	 */
	private static ItemCollection<QueryOutcome> getItemsBasedOnAttributeIndexName(Table table, String value,
			String indexName) {
		Index index = table.getIndex(indexName);
		ItemCollection<QueryOutcome> items = null;
		
		QuerySpec querySpec = new QuerySpec();
		querySpec.withKeyConditionExpression(
				indexName + " = :v_value")
		.withValueMap(new ValueMap().withString(":v_value", value));
		items = index.query(querySpec);
		return items;
	}
	
	private static boolean wasTableFound(Table table) {
		return table != null;
	}



	private static Table getTable(String TableName, DynamoDB dynamoDB) throws InterruptedException {

		Table table = DynamoDBUtils.getTable(dynamoDB, TableName);
		return table;
	}



	
}
