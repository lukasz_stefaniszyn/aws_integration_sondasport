package com.sondasports.account.lambda;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.sondasports.account.db.model.DBItemModel;
import com.sondasports.account.db.settings.DynamoDbClientSettings;
import com.sondasports.account.db.settings.DynamoDbTableSettings;
import com.sondasports.account.lambda.core.DBitem;
import com.sondasports.account.lambda.core.RequestData;
import com.sondasports.account.lambda.core.ResponseData;
import com.sondasports.account.lambda.exceptions.TableGetException;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public class DBitemGetEmail implements RequestHandler<RequestData, ResponseData> {

	private DynamoDB dynamoDB;

	// Initialize the Log4j logger.
	static final Logger log = Logger.getLogger(DBitemGetEmail.class);

	public DBitemGetEmail() {
		this(DynamoDbClientSettings.DYNAMO_DB_CLIENT.getInstance());
	}

	public DBitemGetEmail(AmazonDynamoDBClient dynamoDBClient) {
		dynamoDB = new DynamoDB(dynamoDBClient);
	}

	
	public ResponseData handleRequest(RequestData request, Context context) {

		System.out.println(request.toString());
		
		Table table = null;
		String tableName = DynamoDbTableSettings.TABLE_NAME_ACCOUNT.toString();
		try {
			table = DBitem.getTableObject(dynamoDB, tableName);
		} catch (TableGetException message) {
			return new ResponseData(message.getCause().getMessage());
		}
		
		DBItemModel dBItemModel;
		String requestAttributeName = "";
		try {
			if ((request.getEmailAddress() != null) && (!request.getEmailAddress().isEmpty())){
				requestAttributeName = request.getEmailAddress();
				dBItemModel = DBitem.getDBItemEmail(table, requestAttributeName);
			} else if ((request.getTokenEmail() != null) && (!request.getTokenEmail().isEmpty())){
				requestAttributeName = request.getTokenEmail();
				dBItemModel = DBitem.getDBItemEmailByToken(table, requestAttributeName);
			} else{
				String message = "Neither email nor token were used in request";
				log.info(message);
				return new ResponseData(message);
			}
			
		} catch (JsonParseException e) {
			return new ResponseData(generateExceptionText(request, tableName, requestAttributeName, e));
		} catch (JsonMappingException e) {
			return new ResponseData(generateExceptionText(request, tableName, requestAttributeName, e));
		} catch (IOException e) {
			return new ResponseData(generateExceptionText(request, tableName, requestAttributeName, e));
		}

		String itemJson = "";
		
		if (dBItemModel == null){
			String message = "Given item: ("
					+ requestAttributeName + ") was not found in table: (" + tableName + ").";
			log.info(message);
			return new ResponseData(message);
		}
			
		
		try {
			itemJson = dBItemModel.getJson();
		} catch (JsonProcessingException e) {
			String message = "Unable parse data from Table: (" + tableName + ") for item: ("
					+ requestAttributeName + "). Error: " + e.getStackTrace().toString();
			log.error(message);
			return new ResponseData(message);
		}
		return new ResponseData(itemJson);
	}

	public String generateExceptionText(RequestData request, String TableName, String requestAttributeName, IOException e) {
		String message = "Unable get from Table: (" + TableName + ") item: (" + requestAttributeName
				+ "). Error: " + e.getStackTrace().toString();
		log.fatal(message);
		return message;
	}


}
