/**
 * 
 */
package com.sondasports.account.lambda.exceptions;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public class GetItemEmailException extends Exception {
	public GetItemEmailException() {
		this("");
	}
	
	public GetItemEmailException(String message) {
		super(message);
	}
}
