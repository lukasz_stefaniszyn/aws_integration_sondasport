package com.sondasports.account.lambda;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.sondasports.account.apigateway.settings.ApiGatewayUrl;
import com.sondasports.account.db.model.AttributeNaming;
import com.sondasports.account.db.model.DBItemModel;
import com.sondasports.account.db.settings.DynamoDbClientSettings;
import com.sondasports.account.db.settings.DynamoDbTableSettings;
import com.sondasports.account.db.utils.GenerateToken;
import com.sondasports.account.lambda.core.DBitem;
import com.sondasports.account.lambda.core.RequestData;
import com.sondasports.account.lambda.core.ResponseData;
import com.sondasports.account.lambda.exceptions.GetItemEmailException;
import com.sondasports.account.lambda.exceptions.TableGetException;
import com.sondasports.account.lambda.utils.LambdaUtils;
import com.sondasports.account.ses.SimpleEmailSender;
import com.sondasports.account.ses.settings.MailSettings;
import com.sondasports.account.ses.settings.SesClientSettings;
import com.sondasports.account.ses.settings.SesConnection;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 */
public class DBitemDeleteEmail implements RequestHandler<RequestData, ResponseData> {
	
	private DynamoDB dynamoDB;
	
	private SimpleEmailSender simpleEmailSender;
	
	private GenerateToken generateToken;
	// Initialize the Log4j logger.
	static final Logger log = Logger.getLogger(DBitemDeleteEmail.class);

	
	public DBitemDeleteEmail() {
		this(DynamoDbClientSettings.DYNAMO_DB_CLIENT.getInstance(), SesClientSettings.SES_CLIENT.getInstance());
	}
	
	
	public DBitemDeleteEmail(AmazonDynamoDBClient dynamoDBClient) {
		this(dynamoDBClient,  SesConnection.getCloud_EuWest1_Client());
	}
	
	
	public DBitemDeleteEmail(AmazonDynamoDBClient dynamoDBClient, AmazonSimpleEmailServiceClient sesClient) {
		dynamoDB = new DynamoDB(dynamoDBClient);
		simpleEmailSender = new SimpleEmailSender(sesClient);
		generateToken = new GenerateToken();
		
	}
	
	public ResponseData handleRequest(RequestData request, Context context) {
		
		System.out.println(request.toString());
		
		Table table = null;
		String tableName = DynamoDbTableSettings.TABLE_NAME_ACCOUNT.toString();
		try {
			table = DBitem.getTableObject(dynamoDB, tableName);
		} catch (TableGetException message) {
			return new ResponseData(message.getCause().getMessage());
		}
		
		String emailAddress = null;
		String message = "";
		emailAddress = request.getEmailAddress();
		if ((emailAddress != null) && (!emailAddress.isEmpty())) {
			
			// Get item described by token
			DBItemModel dbItemEmail = null;
			try {
				dbItemEmail = getDbItemModelByEmail(table, emailAddress);
			} catch (GetItemEmailException e) {
				return new ResponseData(e.getMessage());
			}
			
			// If given emailAddress was not found in tabel
			if (null == dbItemEmail) {
				message = "Email (" + emailAddress + ") was not found in table (" + tableName + ").";
				return new ResponseData(message);
			}
			
			// Verify if 'isActive' was already set to true, then end request
			if (false == dbItemEmail.getIsActive()) {
				message = "Email (" + emailAddress + ")  account is not active. Please contact with adminitrator";
				return new ResponseData(message);
			}
			
			// Update 'isActive' to false
			Boolean isActive = false;
			DBitem.updateDBItemIsActive(table, emailAddress, isActive);
			
			// Generate new token
			String newToken = generateToken.getToken();
			DBitem.updateDBItemToken(table, emailAddress, newToken);
			
			// Send information email
			String body = generateEmailBody(emailAddress, newToken);
			simpleEmailSender.sendMail(emailAddress, body);
			
			// DeleteItemOutcome delEmailDBItemModel = DBitem.delDBItemEmail(table, emailAddress);
			// message = "Email (" + emailAddress + ") remove response: " + delEmailDBItemModel.toString();
		} else {
			message = "Request with Email (" + emailAddress + ") was empty. Nothing to remove";
		}
		message = "Email (" + emailAddress + ") account is set to inactive. Reactivation token was sent.";
		return new ResponseData(message);
	}
	
	private String generateEmailBody(String emailAddress, String newToken) {
		List<String> list = Arrays.asList(ApiGatewayUrl.ENDPOINT.toString(), ApiGatewayUrl.STAGE_DEV.toString(),
				ApiGatewayUrl.RESOURCE_URL.toString());
		String tokenUrl = String.join("/", list).concat("?"+ AttributeNaming.tokenName + "=" + newToken); // Generate activation token url
		String body = MailSettings.BODY.toString().replace("${emailAddress}", emailAddress).replace("${tokenUrl}",
				tokenUrl);
		return body;
	}
	
	/**
	 * @param request
	 * @param table
	 * @param tableName
	 * @param emailAddress
	 * @return
	 * @throws GetItemEmailException
	 */
	private DBItemModel getDbItemModelByEmail(Table table, String emailAddress) throws GetItemEmailException {
		DBItemModel dbItemModel = null;
		String tableName = table.getTableName();
		try {
			dbItemModel = DBitem.getDBItemEmail(table, emailAddress);
		} catch (JsonParseException e) {
			throw new GetItemEmailException(LambdaUtils.generateExceptionText(tableName, emailAddress, e));
		} catch (JsonMappingException e) {
			throw new GetItemEmailException(LambdaUtils.generateExceptionText(tableName, emailAddress, e));
		} catch (IOException e) {
			throw new GetItemEmailException(LambdaUtils.generateExceptionText(tableName, emailAddress, e));
		}
		return dbItemModel;
	}
	
}