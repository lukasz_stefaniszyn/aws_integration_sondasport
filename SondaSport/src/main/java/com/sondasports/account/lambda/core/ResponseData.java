package com.sondasports.account.lambda.core;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "response" })
public class ResponseData {

	/*
	 * Reponse will have this structure 
	 * { "response": 
		 *  {"emailAddress":"",
		 * "creationData":"1990.01.01",
		 * "isActive":false,
		 * "token":"zxcv123",
		 * "tokenWasUsed":false} 
	 *  }
	 */

	@JsonProperty("response")
	private String response;

	public ResponseData() {
	}

	public ResponseData(String response) {
		this.response = response;
	}

	@JsonFormat(shape = JsonFormat.Shape.OBJECT)
	@JsonProperty("response")
	public String getResponse() {
		return response;
	}

	@Override
	public String toString() {
		return getResponse();
	}

}
