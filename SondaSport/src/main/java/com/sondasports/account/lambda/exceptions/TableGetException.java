package com.sondasports.account.lambda.exceptions;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
@SuppressWarnings("serial")
public class TableGetException extends Exception{

	public TableGetException() {
		this("");
	}
	
	public TableGetException(String message) {
		super(message);
	}
	
	
}
