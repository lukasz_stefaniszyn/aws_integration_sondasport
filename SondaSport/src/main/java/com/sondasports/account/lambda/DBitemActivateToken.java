package com.sondasports.account.lambda;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.sondasports.account.db.model.DBItemModel;
import com.sondasports.account.db.settings.DynamoDbClientSettings;
import com.sondasports.account.db.settings.DynamoDbTableSettings;
import com.sondasports.account.lambda.core.DBitem;
import com.sondasports.account.lambda.core.RequestData;
import com.sondasports.account.lambda.core.ResponseData;
import com.sondasports.account.lambda.exceptions.GetItemEmailException;
import com.sondasports.account.lambda.exceptions.TableGetException;
import com.sondasports.account.lambda.utils.LambdaUtils;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 */
public class DBitemActivateToken implements RequestHandler<RequestData, ResponseData> {
	
	private DynamoDB dynamoDB;
	
	// Initialize the Log4j logger.
	static final Logger log = Logger.getLogger(DBitemActivateToken.class);
	
	public DBitemActivateToken() {
		this(DynamoDbClientSettings.DYNAMO_DB_CLIENT.getInstance());
	}
	
	public DBitemActivateToken(AmazonDynamoDBClient dynamoDBClient) {
		dynamoDB = new DynamoDB(dynamoDBClient);
	}
	
	public ResponseData handleRequest(RequestData request, Context context) {
		
		System.out.println(request.toString());
		
		Table table = null;
		String tableName = DynamoDbTableSettings.TABLE_NAME_ACCOUNT.toString();
		try {
			table = DBitem.getTableObject(dynamoDB, tableName);
		} catch (TableGetException e) {
			return new ResponseData(e.getMessage());
		}
		
		String tokenEmail = null;
		
		String message = "";
		if ((request.getTokenEmail() != null) && (!request.getTokenEmail().isEmpty())) {
			tokenEmail = request.getTokenEmail();
			
			
			//Get item described by token
			DBItemModel dbItemModel;
			try {
				dbItemModel = getDbItemModelByToken(table, tokenEmail);
			} catch (GetItemEmailException e) {
				return new ResponseData(e.getMessage());
			}
			
			//If given token was not found in tabel 
			if (null == dbItemModel) {
				message = "Given item: (" + tokenEmail + ") was not found in table: (" + tableName + ").";
				return new ResponseData(message);
			}
			
			//Verify if this token was already used 
			if (true == dbItemModel.getTokenWasUsed()) {
				message = "Token (" + tokenEmail + ") already used";
				return new ResponseData(message);
			}
			
			final String emailAddress = dbItemModel.getEmailAddress();

			activateAccount(table, emailAddress);
			setTokenWasUsedToTrue(table, emailAddress);
			
			message = "Email account (" + emailAddress + ") is now active.";
			return new ResponseData(message);
			
		} else {
			message = "Request with token (" + tokenEmail + ") was empty. Nothing to activate";
		}
		System.out.println(message);
		return new ResponseData(message);
	}

	private void setTokenWasUsedToTrue(Table table, final String emailAddress) {
		boolean wasTokenUsed = true;
		DBitem.updateDBItemTokenWasUsed(table, emailAddress, wasTokenUsed);
	}

	private void activateAccount(Table table, final String emailAddress) {
		boolean isActive = true;
		DBitem.updateDBItemIsActive(table, emailAddress, isActive);
	}

	private DBItemModel getDbItemModelByToken(Table table, String tokenEmail) throws GetItemEmailException {
		DBItemModel dbItemModel = null;
		String tableName = table.getTableName();
		try {
			dbItemModel = DBitem.getDBItemEmailByToken(table, tokenEmail);
		} catch (JsonParseException e) {
			throw new GetItemEmailException(LambdaUtils.generateExceptionText(tableName, tokenEmail, e));
		} catch (JsonMappingException e) {
			throw new GetItemEmailException(LambdaUtils.generateExceptionText(tableName, tokenEmail, e));
		} catch (IOException e) {
			throw new GetItemEmailException(LambdaUtils.generateExceptionText(tableName, tokenEmail, e));
		}
		return dbItemModel;
	}

	
}
