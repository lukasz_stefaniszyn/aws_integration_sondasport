package com.sondasports.account.db.settings;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public enum DynamoDbTableSettings {
	TABLE_NAME_ACCOUNT("Accounts"),;
	
	
	private String tableName;

	private DynamoDbTableSettings(String tableName){
		this.tableName = tableName;
		
	}
	
	@Override
	public String toString() {
		return this.tableName;
	}
}
