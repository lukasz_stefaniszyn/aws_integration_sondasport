package com.sondasports.account.db.settings;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public enum DynamoDbClientSettings {
	DYNAMO_DB_CLIENT,;
	
	public AmazonDynamoDBClient getInstance(){
        return new AmazonDynamoDBClient()
        		.withRegion(
        		Region.getRegion(Regions.EU_WEST_1)
        		);
	}
}
