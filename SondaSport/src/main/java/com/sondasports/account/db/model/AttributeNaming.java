package com.sondasports.account.db.model;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public interface AttributeNaming {
	public static final String emailAddressName = "emailAddress";
	public static final String isActiveName = "isActive";
	public static final String creationDataName = "creationData";
	public static final String tokenName = "tokenEmail";
	public static final String tokenWasUsedName = "tokenWasUsed";
}