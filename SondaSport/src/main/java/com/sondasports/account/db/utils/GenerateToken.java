package com.sondasports.account.db.utils;

import java.security.SecureRandom;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 */
public class GenerateToken {
	
	private SecureRandom prng;
	
	public static final String EMPTY_TOKEN_STRING = "none";
	
	public GenerateToken() {
		// Initialize SecureRandom
		// This is a lengthy operation, to be done only upon
		// initialization of the application
		try {
			setPrng(SecureRandom.getInstance("SHA1PRNG"));
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Unable to create RandomNumber. " + e.getMessage());
		}
		
	}
	
	/**
	 * This will return generated activation token, in this format String("14ab7e70329771293ba1bf3f0939b55cc8e1fb83"). 
	 * In case of generation failure You will receive String("none"). 
	 */
	public String getToken() {
		
		byte[] generateRandomNumberValue = generateRandomNumberValue();
		
		if (null == generateRandomNumberValue) {
			return GenerateToken.EMPTY_TOKEN_STRING;
		}
		
		String hexEncode = hexEncode(generateRandomNumberValue);
		return hexEncode;
	}
	
	private byte[] generateRandomNumberValue() {
		byte[] result = null;
		
		if (null == this.getPrng() ) {
			return result;
		}
		
		// generate a random number
		String randomNum = new Integer(getPrng().nextInt()).toString();
		
		try {
			// get its digest
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			result = sha.digest(randomNum.getBytes());
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Unable to get new RandomNumber. " + e.getMessage());
			return result;
		}
		return result;
	}
	
	/**
	 * The byte[] returned by MessageDigest does not have a nice textual representation, so some form of encoding is
	 * usually performed. This implementation follows the example of David Flanagan's book "Java In A Nutshell", and
	 * converts a byte array into a String of hex characters. Another popular alternative is to use a "Base64" encoding.
	 */
	private String hexEncode(byte[] aInput) {
		StringBuilder result = new StringBuilder();
		char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		for (int idx = 0; idx < aInput.length; ++idx) {
			byte b = aInput[idx];
			result.append(digits[(b & 0xf0) >> 4]);
			result.append(digits[b & 0x0f]);
		}
		return result.toString();
	}

	public SecureRandom getPrng() {
		return prng;
	}

	public void setPrng(SecureRandom prng) {
		this.prng = prng;
	}
}
