package com.sondasports.account.db;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.sondasports.credentials.GetAWSCredentials;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public class DataBaseConnection {

	enum DynamoDBClient {
//		## Full list of regions and its endpoints: http://docs.aws.amazon.com/general/latest/gr/rande.html#ddb_region
		LOCAL_INSTANCE(
				"default", 
				"http://127.0.0.1:8000/", 
				null), 
		
		CLOUD_INSTANCE_EU_WEST_1(
				"sondasport_lucst", 
				"dynamodb.eu-west-1.amazonaws.com", 
				Regions.EU_WEST_1);

		private AmazonDynamoDBClient client = null;

		private DynamoDBClient(String credentialsName, String DynamoDBendpoint,Regions region ) {
			
			AWSCredentials credentials = new GetAWSCredentials(credentialsName).getCredentials();
			
			AmazonDynamoDBClient client = new AmazonDynamoDBClient(credentials);
			this.client = client.withEndpoint(DynamoDBendpoint);
			if (region != null){
				Region reg = Region.getRegion(region); 
				this.client.setRegion(reg);
			}

		}

		public AmazonDynamoDBClient getClient() {
			return this.client;
		}
	}
	
	public static AmazonDynamoDBClient getLocalClient() throws AmazonClientException{
		System.out.println("Creating " + DynamoDBClient.LOCAL_INSTANCE.name());
		return DynamoDBClient.LOCAL_INSTANCE.getClient();
	}
	
	
	public static AmazonDynamoDBClient getCloud_EuWest1_Client() throws AmazonClientException{
		System.out.println("Creating " + DynamoDBClient.CLOUD_INSTANCE_EU_WEST_1.name());
		return DynamoDBClient.CLOUD_INSTANCE_EU_WEST_1.getClient();
	}

}
//
// DynamoDB dynamoDB = new DynamoDB(client);
//
// String tableName = "Movies";
//
// try {
// System.out.println("Attempting to create table; please wait...");
// Table table = dynamoDB.createTable(tableName,
// Arrays.asList(new KeySchemaElement("year", KeyType.HASH), // Partition
// // key
// new KeySchemaElement("title", KeyType.RANGE)), // Sort
// // key
// Arrays.asList(new AttributeDefinition("year", ScalarAttributeType.N),
// new AttributeDefinition("title", ScalarAttributeType.S)),
// new ProvisionedThroughput(10L, 10L));
// table.waitForActive();
// System.out.println("Success. Table status: " +
// table.getDescription().getTableStatus());
//
// } catch (Exception e) {
// System.err.println("Unable to create table: ");
// System.err.println(e.getMessage());
// }
//
// }
// }