package com.sondasports.account.db.exceptions;


import com.amazonaws.AmazonClientException;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public class DynamoDBConnectionException extends AmazonClientException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7392734927950079749L;

	public DynamoDBConnectionException(AmazonClientException e) {
		super( getExceutedMethodName(e) );
}

	private static String getExceutedMethodName(AmazonClientException errorStack) {
		
		String exceptionReason = errorStack.getCause().toString().replaceFirst(":", "\n");
		System.err.println(exceptionReason);
		String executedMethodName = "\nExecuted method: ";
		try{
			String stackTraceElements="";
			for (int i=0; i++<16;){
				String stackTraceElement = Thread.currentThread().getStackTrace()[i].toString();
				if (stackTraceElement.contains("com.sondasports.account") && 
					!stackTraceElement.contains("com.sondasports.account.db.exceptions.DynamoDBConnectionException")){
					stackTraceElements = stackTraceElements.concat("\n" + Thread.currentThread().getStackTrace()[i].toString());
				}
			}
			executedMethodName = executedMethodName + stackTraceElements + "\n" ; 
		}catch (ArrayIndexOutOfBoundsException e){
			executedMethodName = "";
		}
		return exceptionReason  + executedMethodName;
	}
	
	
}
