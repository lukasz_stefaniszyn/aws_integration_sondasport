package com.sondasports.account.db.utils;

import java.util.concurrent.TimeUnit;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.TableDescription;



/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public class DynamoDBUtils {

	
	/**
	 * 
	 * @param dynamoDB
	 * @param TableName
	 * @return
	 * @throws InterruptedException
	 */
	public static Table getTable(DynamoDB dynamoDB, String TableName) throws InterruptedException{
		Table table = dynamoDB.getTable(TableName);
		// check if table already exists, and if not wait for it to become active
		TableDescription desc = null;
		try{
			desc = table.waitForActiveOrDelete();
		} catch (IllegalStateException e){
			System.err.println(e);
			return null;
		}
		if (desc != null) {
//			System.out.println("Table ("+ TableName + ") was found");
			return table;
		}
		return null;
	}

	
}
