package com.sondasports.account.db.model;

import java.io.IOException;
import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
@JsonPropertyOrder(value = { AttributeNaming.emailAddressName, AttributeNaming.creationDataName,
		AttributeNaming.isActiveName, AttributeNaming.tokenName, AttributeNaming.tokenWasUsedName })
public class DBItemModel extends Item implements Serializable {

	/**
	 * 
	 */
	@JsonIgnore
	private final long serialVersionUID = 7056716346585663478L;

	@JsonIgnore
	public final static String HASH_KEY_NAME = AttributeNaming.emailAddressName;
	
	@JsonIgnore
	public final static String GLOBAL_HASH_KEY_NAME= AttributeNaming.tokenName;
	
	@JsonIgnore
	private String hashKeyValue = this.getEmailAddress();

	@JsonProperty(AttributeNaming.emailAddressName)
	private String emailAddress = "";
	
	@JsonProperty(AttributeNaming.creationDataName)
	private String creationData = "1990.01.01";
	// private String creationData = new DateTime().toString();
	
	@JsonProperty(AttributeNaming.isActiveName)
	private Boolean isActive = true;

	@JsonProperty(AttributeNaming.tokenName)
	private String token = "zxcv123";
	
	@JsonProperty(AttributeNaming.tokenWasUsedName)
	private Boolean tokenWasUsed = false;

	
	public DBItemModel() {
	}

	public String getHashKeyValue() {
		return hashKeyValue;
	}

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy.MM.dd")
	public String getCreationData() {
		return creationData;
	}
	public void setCreationData(String creationData) {
		this.creationData = creationData;
	}
	
	@JsonFormat(shape = JsonFormat.Shape.BOOLEAN)
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

    @JsonFormat(shape = JsonFormat.Shape.STRING)
	public String getToken() {
		return token;
	}
	private void setToken(String token) {
		this.token = token;
	}

    @JsonFormat(shape = JsonFormat.Shape.BOOLEAN)
	public Boolean getTokenWasUsed() {
		return tokenWasUsed;
	}
	public void setTokenWasUsed(Boolean tokenWasUsed) {
		this.tokenWasUsed = tokenWasUsed;
	}

	@JsonIgnore
	public Item getItem() {

		Item item = new Item()
				.withPrimaryKey(DBItemModel.HASH_KEY_NAME, getEmailAddress())
				.withBoolean(AttributeNaming.tokenWasUsedName, getTokenWasUsed())
				.with(AttributeNaming.creationDataName, getCreationData())
				.withBoolean(AttributeNaming.isActiveName, getIsActive())
				.with(AttributeNaming.tokenName, getToken());

		return item;
	}

	@JsonIgnore
	public String getJson() throws JsonProcessingException {
		return DBItemModel.convertDBItemObjetToJson(this);
	}

	public static DBItemModel convertJsonToDBItemObject(String itemJson)
			throws JsonParseException, JsonMappingException, IOException {

		System.out.println("DBItemModel.convertJsonToDBItemObject()");
		
		ObjectMapper objectMapper = new ObjectMapper();

		// Convert Json to ObjectObject into json
		DBItemModel dbItemModel = objectMapper.readValue(itemJson, DBItemModel.class);
		return dbItemModel;
	}

	public static String convertDBItemObjetToJson(DBItemModel dbItemModel) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();

		// Convert Object into json
		String itemJson = objectMapper.writeValueAsString(dbItemModel);
		return itemJson;
	}

	

}
