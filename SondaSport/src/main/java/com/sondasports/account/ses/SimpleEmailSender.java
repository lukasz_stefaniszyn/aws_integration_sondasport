/**
 * 
 */
package com.sondasports.account.ses;

import org.apache.log4j.Logger;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.sondasports.account.lambda.DBitemActivateToken;
import com.sondasports.account.ses.settings.MailSettings;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 */
public class SimpleEmailSender {
	
	// Initialize the Log4j logger.
	static final Logger log = Logger.getLogger(DBitemActivateToken.class);
	
	private AmazonSimpleEmailServiceClient sesClient;
	

	public SimpleEmailSender(AmazonSimpleEmailServiceClient client) {
		sesClient = client;
	}
	

	public void sendMail(String To, String Body) {
		
		String From = MailSettings.FROM.toString();
		String Subject = MailSettings.SUBJECT.toString();
		this.sendMail(To, From, Subject, Body);
	}

	
	
	public void sendMail(String To, String From, String Subject, String Body) {
		
		SendEmailRequest createEmailRequest = createEmail(To, From, Subject, Body);
		sesClient.sendEmail(createEmailRequest);
		System.out.println("Email sent!");
	}

	private SendEmailRequest createEmail(String To, String From, String Subject, String Body) {
		// Construct an object to contain the recipient address.
		Destination destination = new Destination().withToAddresses(new String[] { To });
		
		// Create the subject and body of the message.
		Content subject = new Content().withData(Subject);
		Content textBody = new Content().withData(Body);
		Body body = new Body().withText(textBody);
		
		// Create a message with the specified subject and body.
		Message message = new Message().withSubject(subject).withBody(body);
		
		// Assemble the email.
		SendEmailRequest request = new SendEmailRequest()
				.withSource(From)
					.withDestination(destination)
					.withMessage(message);
		
		return request;
	}

	
}
