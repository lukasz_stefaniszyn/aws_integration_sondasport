package com.sondasports.account.ses.settings;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.sondasports.credentials.GetAWSCredentials;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public class SesConnection {

	enum SesClient {
//		## Full list of regions and its endpoints: http://docs.aws.amazon.com/general/latest/gr/rande.html#ddb_region
		CLOUD_INSTANCE_EU_WEST_1(
				"sondasport_lucst", 
				Regions.EU_WEST_1);

		private AmazonSimpleEmailServiceClient client = null;

		private SesClient(String credentialsName,Regions region ) {
			
			AWSCredentials credentials = new GetAWSCredentials(credentialsName).getCredentials();
			
			AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(credentials);
			this.client = client;
			if (region != null){
				Region reg = Region.getRegion(region); 
				this.client.setRegion(reg);
			}

		}

		public AmazonSimpleEmailServiceClient getClient() {
			return this.client;
		}
	}
	
	
	public static AmazonSimpleEmailServiceClient getCloud_EuWest1_Client() throws AmazonClientException{
		System.out.println("Creating " + SesClient.CLOUD_INSTANCE_EU_WEST_1.name());
		return SesClient.CLOUD_INSTANCE_EU_WEST_1.getClient();
	}

}
