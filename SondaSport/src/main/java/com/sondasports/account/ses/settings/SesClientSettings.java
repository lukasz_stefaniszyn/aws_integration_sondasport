package com.sondasports.account.ses.settings;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public enum SesClientSettings {
	SES_CLIENT,;
	
	public AmazonSimpleEmailServiceClient getInstance(){
        return new AmazonSimpleEmailServiceClient()
        		.withRegion(
        		Region.getRegion(Regions.EU_WEST_1)
        		);
	}
}
