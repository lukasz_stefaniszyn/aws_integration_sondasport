package com.sondasports.account.ses.settings;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public enum MailSettings {
	FROM("test@softwareautomation.pl"),
	SUBJECT("Your account is deactivated"), 
	BODY("Dear user,\n" 
			+ "Thank You for being with us\n" 
			+ "We have recieved Your resignation request for email account (${emailAddress})\n"
			+ "\n"
			+ "Your account is now disabled.\n"
			+ "You can always reactivate it by clicking this link: \n"
			+ "${tokenUrl}"
			);
	
	private String value;

	private MailSettings(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return this.value;
	}
	
	
}
