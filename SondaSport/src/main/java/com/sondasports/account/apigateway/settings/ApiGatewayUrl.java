package com.sondasports.account.apigateway.settings;

/**
 * @author Lukasz Stefaniszyn <lukasz@softwareautomation.pl>
 *
 */
public enum ApiGatewayUrl {
	ENDPOINT("https://blxfiab4z5.execute-api.eu-west-1.amazonaws.com"),
	STAGE_DEV("dev"), 
	RESOURCE_URL("account/activate");
	
	private String value;

	private ApiGatewayUrl(String value) {
		this.value = value;
	}

	
	@Override
	public String toString() {
		return this.value;
	}
	
}
